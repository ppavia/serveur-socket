package ppa.serversocket;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ClientThreaded implements Runnable {
	private ServerSocket socketServer;
	private Socket socket;
	private int idClient = 1;
	
	public ClientThreaded(ServerSocket ss) {
		this.socketServer	= ss;
	}

	@Override
	public void run() {
		try {
			while ( true ) {
				socket = socketServer.accept();
				System.out.println("client with '" + idClient + "' is connected");
				idClient++;
				socket.close();
			}
		}
		catch (IOException e) {
			e.printStackTrace();
		}		
	}

}
