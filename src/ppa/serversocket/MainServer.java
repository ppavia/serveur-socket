package ppa.serversocket;

import java.io.IOException;
import java.net.ServerSocket;

public class MainServer {
	public static ServerSocket socketServer;
	public static Thread t;
	
//	public static void main (String...args) {		
//		try {
//			socketServer	= new ServerSocket(2009);
//			//socketServer.setSoTimeout(1000);
//			System.out.println("connexion on server : " + socketServer.getInetAddress().getHostAddress() + " : " + socketServer.getLocalPort());
//			t	= new Thread(new ServerSocketConnexion(socketServer));
//			t.start();
//			System.out.println("all clients readies");
//		}
//		catch ( IOException e ) {
//			System.err.println("local port [" + socketServer.getLocalPort() + "]" + " is in use");
//		}
//	}
	
	public static void main (String...args) {
		try {
			socketServer	= new ServerSocket(2009);
			ServerSocketConnexion ssc	= new ServerSocketConnexion(socketServer);
			System.out.println("connexion on server : " + socketServer.getInetAddress().getHostAddress() + " : " + socketServer.getLocalPort());
			System.out.println("all clients readies");
			ssc.run();
		}
		catch ( IOException e ) {
			System.err.println("local port [" + socketServer.getLocalPort() + "]" + " is in use");
		}
	}
}
