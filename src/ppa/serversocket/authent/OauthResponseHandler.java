package ppa.serversocket.authent;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.Date;

import ppa.http.utils.HttpUtils;
import ppa.http.utils.Logger;
import ppa.httpstream.model.HttpStream;

public class OauthResponseHandler {
	private static Logger<OauthResponseHandler> log			= Logger.getLogger(OauthResponseHandler.class);
	private Socket socket;
	private BufferedReader in;
	private PrintWriter out;
	public Thread thread;
	private BufferedInputStream bis;
	
	public OauthResponseHandler(Socket socket) {
		this.socket	= socket;
	}
	
	public void run() {
		try {
			bis	= new BufferedInputStream(socket.getInputStream());
			in	= new BufferedReader(new InputStreamReader(socket.getInputStream()));
			out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())),true);
			if ( in == null ) {
				System.err.println("OauthResponseHandler : BufferedReader in is null !!");
			}
			if ( out == null ) {
				System.err.println("OauthResponseHandler : PrintWriter out is null !!");
			}
			log.info("socket : " + socket.getReceiveBufferSize());
			log.info("socket : " + socket.getKeepAlive());

			HttpStream httpStream	= HttpUtils.readStream(bis);
			
			log.info("httpStream : \n" + httpStream.toString());
			
			if ( httpStream != null ) {
				String response	= mockJsonContentResponse();				
				out.print(response);
				out.println();
				out.flush();
				log.info("response : \n" + response);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			if ( in != null ) {
				try {
					in.close();
					log.info("in : close");
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
			if ( out != null ) {
				try {
					out.close();
					log.info("out : close");
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public String mockJsonContentResponse () {
		// content
		StringBuilder content	= new StringBuilder("");
		content.append("{");
		content.append("\"access_token\"").append(":").append("{");
		content.append("\"uid\"").append(":").append("H66319").append(",");
		content.append("\"mail\"").append(":").append("ppavia.externe@macif.fr").append(",");
		content.append("\"cdCentre\"").append(":").append("27").append(",");
		content.append("\"roles\"").append(":").append("").append(",");
		content.append("\"pi.sri\"").append(":").append("eMleueSn_33qBC3gFq_fyfuJx40..zXlJ").append(",");
		content.append("\"region\"").append(":").append("SIEGE").append(",");
		content.append("\"nom\"").append(":").append("PAVIA").append(",");
		content.append("\"prenom\"").append(":").append("Pierre").append(",");
		content.append("\"fonctionRH\"").append(":").append("Autre Fonction").append("");
		content.append("}").append(",");
		content.append("\"scope\"").append(":").append("openid").append(",");
		content.append("\"token_type\"").append(":").append("urn:").append(",");
		content.append("\"expires_in\"").append(":").append(7159).append(",");
		content.append("\"client_id\"").append(":").append("H66319").append("");
		content.append("}");
		
		byte[] postData			= content.toString().getBytes( StandardCharsets.UTF_8 );
		int    postDataLength	= postData.length;
		
		StringBuilder response	= new StringBuilder("HTTP/1.0 200 OK").append(HttpUtils.CRLF);
		response.append("Content-Length").append(":").append(postDataLength).append(HttpUtils.CRLF);
		response.append("Content-Type").append(":").append("application/json").append(HttpUtils.CRLF);
		response.append("HTTP-date").append(":").append(new Date()).append(HttpUtils.CRLF);
		response.append(HttpUtils.CRLF);
		response.append(content.toString());
		response.append(HttpUtils.CRLF);
		
		return response.toString();
	}
}
