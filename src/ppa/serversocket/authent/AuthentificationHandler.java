package ppa.serversocket.authent;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class AuthentificationHandler implements Runnable {
	private Socket socket;
	private BufferedReader in;
	private PrintWriter out;
	private String login;
	private String password;
	private boolean isAuthentified	= false;
	public Thread thread;
	
	public AuthentificationHandler(Socket socket) {
		this.socket	= socket;
	}
	
	@Override
	public void run() {
		try {
			in	= new BufferedReader(new InputStreamReader(socket.getInputStream()));
			out = new PrintWriter(socket.getOutputStream());
			if ( in == null ) {
				System.err.println("AuthentificationHandler : BufferedReader in is null !!");
			}
			if ( out == null ) {
				System.err.println("AuthentificationHandler : PrintWriter out is null !!");
			}
			out.println(AuthentMessage.LOGIN);
			out.flush();
			login	= in.readLine();
			
			out.println(AuthentMessage.PASSWORD);
			out.flush();
			password	= in.readLine();
			
			if ( isValid(login, password) ) {
				out.println(AuthentMessage.CONNECTED.getMessage());
				out.flush();
				isAuthentified	= true;
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}	
	}
	
	private boolean isValid (String login, String password) {
		
		try ( Scanner sc = new Scanner(new File("datas/credentials.txt")) ){
			while ( sc.hasNext() ) {
				if ( sc.nextLine().equals(login + ":" + password)) {
					return true;
				}
			}
		}
		catch ( FileNotFoundException e ) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean isAuthentified() {
		return isAuthentified;
	}
}
